# Lint Your Code

This repository uses consistent PHP code style throughout, and enforces it using [PHPCS](https://github.com/squizlabs/PHP_CodeSniffer). The standards in use are the [Chance Digital coding standards](https://github.com/chancedigital/wp-coding-standards) and are referenced in the `phpcs.ruleset.xml` file.

To lint your code, make sure you have `phpcs` installed, as well as the WordPress coding standards. Once you do, run `yarn lint` to lint your code.

## Install

To lint your PHP code, you'll need to install PHPCS and the WordPress coding standards.

### PHPCS

To install the latest stable version of PHPCS with Homebrew on the Mac, run:

`brew install homebrew/php/php-code-sniffer`

### WordPress Coding Standards

To install the correct standards, follow the instructions [here](https://github.com/chancedigital/wp-coding-standards).

## Run the Linter

Run the linter at the command line using the following command:

`yarn lint`

Note that the linter only checks the headless theme and Robo file. It does not lint all of WordPress.
