# Dash Financial Headless WordPress

A WordPress backend that serves its data via the [WP REST API](https://developer.wordpress.org/rest-api/).

## Environment variables

You should have a `.env.php` file in the `wordpress` directory where the following constants are defined (values are included as examples and will vary depending on your environment):

```php
define( 'DB_NAME',          'wp_sample' );
define( 'DB_NAME',          'wp_sample' );
define( 'DB_USER',          'wp_sample' );
define( 'DB_PASSWORD',      'wp_sample' );
define( 'DB_HOST',          '0.0.0.0' );
define( 'DB_CHARSET',       'utf8' );
define( 'DB_COLLATE',       '' );
define( 'WP_DEBUG',         true );
define( 'WP_ENV',           'development' );
define( 'DASH_DEV_URL',     'http://localhost:3000' );
define( 'DASH_STAGE_URL',   'https://dashfinancial.now.sh' );
define( 'DASH_PROD_URL',    'http://www.dashfinancial.com' );
define( 'AUTH_KEY',         'KEY VALUE HERE' );
define( 'SECURE_AUTH_KEY',  'KEY VALUE HERE' );
define( 'LOGGED_IN_KEY',    'KEY VALUE HERE' );
define( 'NONCE_KEY',        'KEY VALUE HERE' );
define( 'AUTH_SALT',        'KEY VALUE HERE' );
define( 'SECURE_AUTH_SALT', 'KEY VALUE HERE' );
define( 'LOGGED_IN_SALT',   'KEY VALUE HERE' );
define( 'NONCE_SALT',       'KEY VALUE HERE' );
define( 'WP_TABLE_PREFIX',  'wp_' );
```
