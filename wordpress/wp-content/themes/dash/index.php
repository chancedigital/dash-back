<?php
// Redirect individual post and pages to the REST API endpoint
if ( is_singular( 'post' ) ) {
	header( 'Location: /wp-json/wp/v2/posts/' . get_post()->ID );
} elseif ( is_page() ) {
	header( 'Location: /wp-json/wp/v2/pages/' . get_queried_object()->ID );
} elseif ( is_singular( 'team' ) ) {
	header( 'Location: /wp-json/wp/v2/team/' . get_queried_object()->ID );
} elseif ( is_single() ) {
	header( 'Location: /wp-json/wp/v2/posts/' . get_post()->ID );
} else {
	header( 'Location: /wp-json/' );
}
