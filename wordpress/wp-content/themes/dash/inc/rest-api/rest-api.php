<?php

namespace Dash\Rest_API;

add_action( 'rest_api_init', __NAMESPACE__ . '\\allow_get_requests_from_any_origin', 15 );
add_action( 'rest_api_init', __NAMESPACE__ . '\\register_rest_api_routes' );
add_action( 'rest_api_init', __NAMESPACE__ . '\\create_api_post_fields' );

function send_contact_form( WP_REST_Request $request ) {
	$demo    = sanitize_text_field( trim( $request['demo'] ) );
	$name    = sanitize_text_field( trim( $request['first_name'] ) );
	$company = sanitize_text_field( trim( $request['company'] ) );
	$email   = sanitize_email( trim( $request['email'] ) );

	$errors = [];
	if ( empty( $name ) ) {
		$errors[] = 'Name is a required field.';
	}
	if ( empty( $email ) ) {
		$errors[] = 'Email is a required field.';
	}
	if ( ! empty( $errors ) ) {
		return new WP_Error( 'contact_form_errors', $errors, [ 'status' => 422 ] );
	}
	$message = '';
	$i       = 0;
	foreach ( [
		'Name'    => $name,
		'Email'   => $email,
		'Company' => $company,
		'Demo'    => $demo,
	] as $label => $field ) {
		if ( ! empty( $field ) ) {
			if ( $i > 0 ) {
				$message .= '<br>';
			}
			$message .= "<strong>$label:</strong> $field";
			$i++;
		}
	}
	$headers = [ 'Content-Type: text/html; charset=UTF-8' ];
	wp_mail( 'chance@josiahgoff.com', 'Demo request form submitted', $message, $headers );
	return 'success';
}

function create_api_post_fields() {
	register_rest_field( 'post', 'post_categories', [
		'get_callback' => __NAMESPACE__ . '\\get_post_cats_for_api',
	] );
	register_rest_field( 'post', 'post_year', [
		'get_callback' => __NAMESPACE__ . '\\get_post_year_for_api',
	] );
}

function get_post_cats_for_api( $object ) {
	$post_id = $object['id'];
	return wp_get_post_categories( $post_id, [
		'fields' => 'all',
	] );
}

function get_post_year_for_api( $object ) {
	$post_id = $object['id'];
	return get_the_date( 'Y', $post_id );
}

/**
 * Placeholder function for determining the frontend origin.
 *
 * @return str Frontend origin URL, i.e., http://localhost:3000.
 * @see        /.env.php
 */
function get_frontend_origin() {
	if ( defined( 'WP_ENV' ) ) {
		if ( WP_ENV === 'development' ) {
			return DASH_DEV_URL;
		}
		if ( WP_ENV === 'staging' ) {
			return DASH_STAGE_URL;
		}
	}
	return DASH_PROD_URL;
}

/**
 * Helper function to get registered post types for routes.
 */
function get_post_types_for_routes() {
	// Get list of available post types, then filter out some we don't need.
	$post_types = get_post_types();
	if ( is_array( $post_types ) ) {
		$post_types = array_filter( $post_types, function( $post_type ) {
			return ! in_array( $post_type, [
				'attachment',
				'revision',
				'nav_menu_item',
				'custom_css',
				'customize_changeset',
				'oembed_cache',
				'user_request',
				'acf-field-group',
				'acf-field',
			], true );
		} );
	} else {
		$post_types = [ 'post', 'page', 'team', 'product' ];
	}
	return $post_types;
}

/**
 * Allow GET requests from * origin
 */
function allow_get_requests_from_any_origin() {

	remove_filter( 'rest_pre_serve_request', 'rest_send_cors_headers' );

	add_filter( 'rest_pre_serve_request', function ( $value ) {
		// header( 'Access-Control-Allow-Origin: ' . get_frontend_origin() );
		header( 'Access-Control-Allow-Origin: *' );
		header( 'Access-Control-Allow-Methods: GET' );
		header( 'Access-Control-Allow-Credentials: true' );
		return $value;
	} );
}

/**
 * Register custom REST API routes.
 */
function register_rest_api_routes() {

	// Define API endpoint arguments
	$slug_arg = [
		'validate_callback' => function ( $param, $request, $key ) {
			return( is_string( $param ) );
		},
	];

	$post_slug_arg = array_merge(
		$slug_arg,
		[ 'description' => 'String representing a valid WordPress post slug' ]
	);

	$page_slug_arg = array_merge(
		$slug_arg,
		[ 'description' => 'String representing a valid WordPress page slug' ]
	);

	// Register post routes
	register_rest_route( 'dash/v1', '/post', [
		'methods'  => 'GET',
		'callback' => __NAMESPACE__ . '\\rest_get_post',
		'args' => [
			'slug' => array_merge(
				$post_slug_arg,
				[
					'required' => true,
				]
			),
		],
	] );

	// Register team members.
	register_rest_route( 'dash/v1', '/team', [
		'methods'  => 'GET',
		'callback' => __NAMESPACE__ . '\\rest_get_team',
		'args' => [
			'slug' => array_merge(
				$post_slug_arg,
				[
					'required' => true,
				]
			),
		],
	] );

	// Register products.
	register_rest_route( 'dash/v1', '/product', [
		'methods'  => 'GET',
		'callback' => __NAMESPACE__ . '\\rest_get_product',
		'args' => [
			'slug' => array_merge(
				$post_slug_arg,
				[
					'required' => true,
				]
			),
		],
	] );

	// Register page routes.
	register_rest_route( 'dash/v1', '/page', [
		'methods'  => 'GET',
		'callback' => __NAMESPACE__ . '\\rest_get_page',
		'args' => [
			'slug' => array_merge(
				$page_slug_arg,
				[
					'required' => true,
				]
			),
		],
	] );

	register_rest_route( 'dash/v1', '/post/preview', [
		'methods'  => 'GET',
		'callback' => __NAMESPACE__ . '\\rest_get_post_preview',
		'args' => [
			'id' => [
				'validate_callback' => function ( $param, $request, $key ) {
					return ( is_numeric( $param ) );
				},
				'required' => true,
				'description' => 'Valid WordPress post ID',
			],
		],
		'permission_callback' => function () {
			return current_user_can( 'edit_posts' );
		},
	] );
}

/**
 * Respond to a REST API request to get post data.
 *
 * @param  WP_REST_Request $request Request.
 * @return WP_REST_Response
 */
function rest_get_post( \WP_REST_Request $request ) {
	return rest_get_content( $request, 'post', __FUNCTION__ );
}

/**
 * Respond to a REST API request to get custom team post data.
 *
 * @param  WP_REST_Request $request Request.
 * @return WP_REST_Response
 */
function rest_get_team( \WP_REST_Request $request ) {
	return rest_get_content( $request, 'team', __FUNCTION__ );
}

/**
 * Respond to a REST API request to get custom product post data.
 *
 * @param  WP_REST_Request $request Request.
 * @return WP_REST_Response
 */
function rest_get_product( \WP_REST_Request $request ) {
	return rest_get_content( $request, 'product', __FUNCTION__ );
}

/**
 * Respond to a REST API request to get page data.
 *
 * @param  WP_REST_Request $request Request.
 * @return WP_REST_Response
 */
function rest_get_page( \WP_REST_Request $request ) {
	return rest_get_content( $request, 'page', __FUNCTION__ );
}

/**
 * Respond to a REST API request to get post or page data.
 * * Handles changed slugs
 * * Doesn't return posts whose status isn't published
 * * Redirects to the admin when an edit parameter is present
 *
 * @param  WP_REST_Request $request Request
 * @param  str             $type Type
 * @param  str             $function_name Function name
 * @return WP_REST_Response
 */
function rest_get_content( \WP_REST_Request $request, $type, $function_name ) {

	$post_types = get_post_types_for_routes();

	$content_in_array = in_array(
		$type,
		$post_types,
		true
	);
	if ( ! $content_in_array ) {
		$type = 'post';
	}
	$slug = $request->get_param( 'slug' );
	$post = get_content_by_slug( $slug, $type );
	if ( ! $post ) {
		return new \WP_Error(
			$function_name,
			$slug . ' ' . $type . ' does not exist',
			[
				'status' => 404,
			]
		);
	};

	// Shortcut to WP admin page editor
	$edit = $request->get_param( 'edit' );
	if ( 'true' === $edit ) {
		header( 'Location: /wp-admin/post.php?post=' . $post->ID . '&action=edit' );
		exit;
	}

	$controller = new \WP_REST_Posts_Controller( 'post' );
	$data       = $controller->prepare_item_for_response( $post, $request );
	$response   = $controller->prepare_response_for_collection( $data );

	return new \WP_REST_Response( $response );
}

/**
 * Returns a post or page given a slug. Returns false if no post matches.
 *
 * @param str $slug Slug
 * @param str $type Valid values are 'post' or 'page'
 * @return Post
 */
function get_content_by_slug( $slug, $type = 'post' ) {

	$post_types = get_post_types_for_routes();

	$content_in_array = in_array(
		$type,
		$post_types,
		true
	);
	if ( ! $content_in_array ) {
		$type = 'post';
	}

	// Use `page_on_front` as slug to get the homepage set within WP.
	if ( $slug === 'page_on_front' ) {
		$type = 'page';
		$slug = get_post_field( 'post_name', get_option( 'page_on_front' ) );
	}

	$args = [
		'name'        => $slug,
		'post_type'   => $type,
		'post_status' => 'publish',
		'numberposts' => 1,
	];

	// phpcs:ignore WordPress.VIP.RestrictedFunctions.get_posts_get_posts
	$post_search_results = get_posts( $args );

	if ( ! $post_search_results ) { // Maybe the slug changed
		// check wp_postmeta table for old slug
		$args  = [
			// phpcs:ignore WordPress.VIP.SlowDBQuery.slow_db_query_meta_query
			'meta_query' => [
				[
					'key' => '_wp_old_slug',
					'value' => $post_slug,
					'compare' => '=',
				],
			],
		];
		$query = new \WP_Query( $args );

		$post_search_results = $query->posts;
	}
	if ( isset( $post_search_results[0] ) ) {
		return $post_search_results[0];
	}
	return false;
}

/**
 * Respond to a REST API request to get a post's latest revision.
 * * Requires a valid _wpnonce on the query string
 * * User must have 'edit_posts' rights
 * * Will return draft revisions of even published posts
 *
 * @param  WP_REST_Request $request Rest request.
 * @return WP_REST_Response
 */
function rest_get_post_preview( \WP_REST_Request $request ) {

	$post_id = $request->get_param( 'id' );
	// Revisions are drafts so here we remove the default 'publish' status
	remove_action( 'pre_get_posts', 'set_default_status_to_publish' );
	$check_enabled = [
		'check_enabled' => false,
	];

	$revisions = wp_get_post_revisions( $post_id, $check_enabled );
	$post      = get_post( $post_id );

	if ( $revisions ) {
		$last_revision = reset( $revisions );
		$rev_post      = wp_get_post_revision( $last_revision->ID );
		$controller    = new \WP_REST_Posts_Controller( 'post' );
		$data          = $controller->prepare_item_for_response( $rev_post, $request );
	} elseif ( $post ) { // There are no revisions, just return the saved parent post
		$controller = new \WP_REST_Posts_Controller( 'post' );
		$data       = $controller->prepare_item_for_response( $post, $request );
	} else {
		$not_found = [
			'status' => 404,
		];

		$error = new \WP_Error(
			'rest_get_post_preview',
			'Post ' . $post_id . ' does not exist',
			$not_found
		);
		return $error;
	}
	$response = $controller->prepare_response_for_collection( $data );
	return new \WP_REST_Response( $response );
}
