<?php

namespace Dash\Admin;

add_filter( 'wp_terms_checklist_args', __NAMESPACE__ . '\\taxonomy_checklist_checked_ontop_filter' );
add_filter( 'preview_post_link', __NAMESPACE__ . '\\set_headless_preview_link' );

/**
 * By default, in Add/Edit Post, WordPress moves checked categories
 * to the top of the list and unchecked to the bottom. When you have
 * subcategories that you want to keep below their parents at all
 * times, this makes no sense. This function removes automatic
 * reordering so the categories widget retains its order regardless
 * of checked state. https://stackoverflow.com/a/12586404
 *
 * @param  arr $args Array of arguments.
 * @return arr
 */
function taxonomy_checklist_checked_ontop_filter( $args ) {
	$args['checked_ontop'] = false;
	return $args;
}

/**
 * Customize the preview button in the WordPress admin to point to the headless client.
 *
 * @param  str $link The WordPress preview link.
 * @return str The headless WordPress preview link.
 */
function set_headless_preview_link( $link ) {
	return \Dash\Rest_API\get_frontend_origin() . '/'
		. '_preview/'
		. get_the_ID() . '/'
		. wp_create_nonce( 'wp_rest' );
}
