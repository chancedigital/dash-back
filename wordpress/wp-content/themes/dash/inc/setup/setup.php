<?php

namespace Dash\Setup;

add_action( 'after_setup_theme', __NAMESPACE__ . '\\theme_setup' );

/**
 * Theme setup.
 */
function theme_setup() {

	// Register navigation menus.
	register_nav_menu( 'header-menu', __( 'Header Menu', 'dash' ) );
	register_nav_menu( 'mobile-menu', __( 'Mobile Menu', 'dash' ) );
	register_nav_menu( 'disclosures-menu', __( 'Disclosures Menu', 'dash' ) );

	// Add support for thumbnails.
	add_theme_support( 'post-thumbnails' );
}
