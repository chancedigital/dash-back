<?php

// Useful global constants.
define( 'DASH_VERSION', '1.0.0' );
define( 'DASH_PATH', get_template_directory() . '/' );
define( 'DASH_INC', DASH_PATH . 'inc/' );
define( 'DASH_NAMESPACE', 'Dash' );
