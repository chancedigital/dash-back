<?php

namespace Dash\Taxonomies;

class Taxonomy {

	private $slug         = '';
	protected $post_types = [];
	protected $labels     = [];
	protected $args       = [];

	public function __construct( $slug, $post_types ) {
		$this->set_slug( $slug );
		$this->set_post_types( $post_types );
		$this->set_labels();
		$this->set_args();
	}

	private function set_slug( $slug ) {
		$this->slug = str_replace( '-', '_', sanitize_key( $slug ) );
	}

	private function set_post_types( $post_types ) {
		$this->post_types = $post_types;
	}

	protected function set_labels() {
		$this->labels = [];
	}

	protected function set_args() {
		$this->args = [];
	}

	protected function get_post_types() {
		return $this->post_types;
	}

	protected function get_labels() {
		return $this->labels;
	}

	protected function get_args() {
		return $this->args;
	}

	public function register() {
		if ( ! empty( $this->slug ) && ! empty( $this->post_types ) && ! empty( $this->args ) && ! empty( $this->labels ) ) {
			$args           = $this->args;
			$args['labels'] = $this->labels;
			register_taxonomy( $this->slug, $this->post_types, $args );
		}
	}
}
