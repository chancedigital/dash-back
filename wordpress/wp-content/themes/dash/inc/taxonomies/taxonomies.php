<?php

namespace Dash\Taxonomies;

add_action( 'init', __NAMESPACE__ . '\\register_taxonomies' );

/**
 * Register our post types.
 * Call the Factory::build method to add post types after creating your new post type class.
 * Post type classes must be added to the `types` sub-directory and a `Types` sub-namespace.
 */
function register_taxonomies() {
	/**
	 * Factory::build()
	 *
	 * @param $type string Class name.
	 * @param $slug string Post type slug.
	 */
	Factory::build( 'product-category', 'product_category', [ 'product' ] );
}
