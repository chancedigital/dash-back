<?php

namespace Dash\Taxonomies\Taxes;

use Dash\Taxonomies\Taxonomy;

class Product_Category extends Taxonomy {

	protected function set_labels() {
		$this->labels = [
			'name'              => _x( 'Product Categories', 'taxonomy general name', 'dash' ),
			'singular_name'     => _x( 'Product Category', 'taxonomy singular name', 'dash' ),
			'search_items'      => __( 'Search Categories', 'dash' ),
			'all_items'         => __( 'All Categories', 'dash' ),
			'parent_item'       => __( 'Parent Category', 'dash' ),
			'parent_item_colon' => __( 'Parent Category:', 'dash' ),
			'edit_item'         => __( 'Edit Category', 'dash' ),
			'update_item'       => __( 'Update Category', 'dash' ),
			'add_new_item'      => __( 'Add New Category', 'dash' ),
			'new_item_name'     => __( 'New Category Name', 'dash' ),
			'menu_name'         => __( 'Category', 'dash' ),
		];
	}

	protected function set_args() {
		$this->args = [
			'hierarchical'      => true,
			'public'            => true,
			'show_ui'           => true,
			'show_admin_column' => true,
			'query_var'         => true,
			'show_in_rest'      => true,
		];
	}
}
