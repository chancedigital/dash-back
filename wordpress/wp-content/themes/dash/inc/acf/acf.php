<?php

namespace Dash\ACF;

// Add a custom options page to associate ACF fields with
if ( function_exists( '\\acf_add_options_page' ) ) {
	\acf_add_options_page( [
		'page_title' => 'Theme Settings',
		'menu_title' => 'Theme Settings',
		'menu_slug'  => 'theme-settings',
		'post_id'    => 'theme-settings',
		'redirect'   => false,
	] );
}
