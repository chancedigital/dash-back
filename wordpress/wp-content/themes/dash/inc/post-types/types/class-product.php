<?php

namespace Dash\Post_Types\Types;

use Dash\Post_Types\Post_Type;

class Product extends Post_Type {

	protected function set_labels() {
		$this->labels = [
			'name'               => _x( 'Products', 'post type general name', 'dash' ),
			'singular_name'      => _x( 'Product', 'post type singular name', 'dash' ),
			'menu_name'          => _x( 'Products', 'admin menu', 'dash' ),
			'name_admin_bar'     => _x( 'Product', 'add new on admin bar', 'dash' ),
			'add_new'            => _x( 'Add New', 'location', 'dash' ),
			'add_new_item'       => __( 'Add New Product', 'dash' ),
			'new_item'           => __( 'New Product', 'dash' ),
			'edit_item'          => __( 'Edit Product', 'dash' ),
			'view_item'          => __( 'View Product', 'dash' ),
			'all_items'          => __( 'All Products', 'dash' ),
			'search_items'       => __( 'Search Products', 'dash' ),
			'parent_item_colon'  => __( 'Parent Product:', 'dash' ),
			'not_found'          => __( 'No Products found.', 'dash' ),
			'not_found_in_trash' => __( 'No Products found in Trash.', 'dash' ),
		];
	}

	protected function set_args() {
		$this->args = [
			'description'        => __( 'Description.', 'dash' ),
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'has_archive'        => false,
			'hierarchical'       => false,
			'menu_position'      => null,
			'query_var'          => true,
			'capability_type'    => 'post',
			'show_in_rest'       => true,
			'taxonomies'         => [ 'product_category' ],
			'supports'           => [ 'title', 'editor', 'thumbnail' ],
			'menu_icon'          => 'dashicons-cart',
		];
	}
}
