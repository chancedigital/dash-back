<?php

namespace Dash\Post_Types\Types;

use Dash\Post_Types\Post_Type;

class Team extends Post_Type {

	protected function set_labels() {
		$this->labels = [
			'name'               => _x( 'Team', 'post type general name', 'dash' ),
			'singular_name'      => _x( 'Team Member', 'post type singular name', 'dash' ),
			'menu_name'          => _x( 'Team', 'admin menu', 'dash' ),
			'name_admin_bar'     => _x( 'Team Member', 'add new on admin bar', 'dash' ),
			'add_new'            => _x( 'Add New', 'location', 'dash' ),
			'add_new_item'       => __( 'Add New Team Member', 'dash' ),
			'new_item'           => __( 'New Team Member', 'dash' ),
			'edit_item'          => __( 'Edit Team Member', 'dash' ),
			'view_item'          => __( 'View Team Member', 'dash' ),
			'all_items'          => __( 'All Team Members', 'dash' ),
			'search_items'       => __( 'Search Team', 'dash' ),
			'parent_item_colon'  => __( 'Parent Team Member:', 'dash' ),
			'not_found'          => __( 'No Team Members found.', 'dash' ),
			'not_found_in_trash' => __( 'No Team Members found in Trash.', 'dash' ),
		];
	}

	protected function set_args() {
		$this->args = [
			'description'        => __( 'Description.', 'dash' ),
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'has_archive'        => false,
			'hierarchical'       => false,
			'menu_position'      => null,
			'query_var'          => true,
			'capability_type'    => 'post',
			'show_in_rest'       => true,
			'supports'           => [ 'title', 'editor', 'thumbnail' ],
			'menu_icon'          => 'dashicons-groups',
		];
	}
}
