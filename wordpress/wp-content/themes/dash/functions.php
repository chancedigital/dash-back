<?php
/**
 * WP theme functions
 *
 * @package dash
 */

namespace Dash;

// Constants
require_once __DIR__ . '/inc/constants.php';

// Composer classes.
$composer_autoload = 'vendor/autoload.php';
if ( defined( 'WP_ENV' ) && WP_ENV === 'development' && file_exists( DASH_PATH . "../$composer_autoload" ) ) {
	require_once DASH_PATH . "../$composer_autoload";
} elseif ( file_exists( DASH_PATH . $composer_autoload ) ) {
	require_once DASH_PATH . $composer_autoload;
}

// Import function files.
$files = [
	'util',
	'autoload',
	'post-types',
	'taxonomies',
	'rest-api',
	'admin',
	'acf',
	'setup',
];
foreach ( $files as $file ) {
	require_once __DIR__ . "/inc/$file/$file.php";
}
